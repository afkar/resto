'use strict';


/**
 * Utils, Library
 */
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var db = require('../../knex/knex');
var utils = require('../utils/utils');

/**
 * @route GET /resepall
 * @group resep
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

 exports.resepall = (req,res)=>{
     var body;
     db.from('MS_Resep').select('MS_Resep.Id','MS_Resep.Resep_name','MS_User.Username','MS_Category.Category_name')
     .join('MS_User','MS_Resep.Creator_user','=','MS_User.Id')
     .join('MS_Category','MS_Resep.Id_category','=','MS_Category.Id')
     .where('MS_Resep.Isdeleted','=',false)
     .andWhere('MS_User.IsDeleted','=',false)
     .andWhere('MS_Category.Isdeleted','=',false)
     .then((response)=>{
        body = { success: true, message: 'success', data: response};
    }).catch((error)=>{
        body = { success: false, message: 'error select', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
     })
 }

 /**
  * @route POST /resepadd
  * @group resep
  * @param {string} resep_name.query.required - Resep Name
  * @param {integer} id_category.query.required - Id Category
  * @param {integer} creator_user.query.required - Creator User
  */

 exports.resepadd = (req,res)=>{
    var body;
    var dataResep = {
        Resep_name : req.query.resep_name,
        Creator_user : req.query.creator_user,
        Isdeleted : false,
        Id_category : req.query.id_category
    }
    db('MS_Resep').insert(dataResep)
    .then((response)=>{
       body = { success: true, message: 'success', data: response }
   }).catch((error)=>{
       body = { success: false, message: 'error insert', data: error}
   }).finally(()=>{
       utils.sendStatus(res,200,body);
    })
}

/**
 * @route GET /resepdetails
 * @group resep
 * @param {integer} id_resep.query.required - Id Resep
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded 
 */

 exports.resepdetails = (req,res)=>{
   var body;
   db.from('MS_Resep').select('MS_Resep.Id','MS_Resep.Resep_name','MS_Category.Category_name')
   .join('MS_Category','MS_Resep.Id_category','=','MS_Category.Id')
   .where('MS_Resep.Id','=',req.query.id_resep)
   .then((response)=>{        
       body = { success: true, message: 'success', data: response};

   }).catch((error)=>{
       body = { success: false, message: 'error select', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
   })
   
 }

 /**
 * @route PUT /resepupdate
 * @group resep
 * @param {integer} id_resep.query.required - Id Resep
 * @param {string} resep_name.query.required - Resep Name
 * @param {integer} id_category.query.required - Id Category
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded 
 */

exports.resepupdate = (req,res)=>{
   var body;
   var dtResep = {
    Resep_name : req.query.resep_name,
    Id_category : req.query.id_category
   } 
   var resep_id = req.query.id_resep;
   db('MS_Resep')
   .where('MS_Resep.Id','=',resep_id)
   .update(dtResep)
   .then((response)=>{        
       body = { success: true, message: 'success', data: response};

   }).catch((error)=>{
       body = { success: false, message: 'error update', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
   })
   
 }

   /**
 * @route DELETE /resepdelete
 * @group resep
 * @param {integer} id_resep.query.required - Id Resep
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded 
 */

exports.resepdelete = (req,res)=>{
   var body;
   var resep_id = req.query.id_resep;
   db('MS_Resep')
   .where('MS_Resep.Id','=',resep_id)
   .update({Isdeleted : true})
   .then((response)=>{        
       body = { success: true, message: 'success deleted', data: response};

   }).catch((error)=>{
       body = { success: false, message: 'error deleted', data: error.message};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
   })
   
 }


/**
 * @route GET /resepbycategory
 * @group resep
 * @param {integer} id_category.query.required - Id Category
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

exports.resepbycategory = (req,res)=>{
    var body;
    var id = req.query.id_category;
    db.from('MS_Resep').select('MS_Category.Category_name','MS_Resep.Resep_name','MS_User.Username')
    .join('MS_Category','MS_Resep.Id_category','=','MS_Category.Id')
    .join('MS_User','MS_Resep.Creator_user','=','MS_User.Id')
    .where('MS_Category.Id','=',id)
    .then((response)=>{
       body = { success: true, message: 'success', data: response};
   }).catch((error)=>{
       body = { success: false, message: 'error select', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
    })
}


/**
 * @route GET /resepdetailall
 * @group resep
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

exports.resepdetailall = (req,res)=>{
    var body;
    db.from('TR_ResepDetail').select('MS_Resep.Resep_name','MS_Ingredients.Ingredients_name','TR.ResepDetail.Total','MS_Category.Category_name')
    .join('MS_User','MS_Resep.Creator_user','=','MS_User.Id')
    .join('MS_Resep','TR_ResepDetail.Id_Resep','=','MS_Resep.Id')
    .join('MS_Ingredients','TR_ResepDetail.Id_Ingredients','=','MS_Ingredients.Id')
    .join('MS_Category','MS_Resep.Id_category','=','MS_Category.Id')
    .then((response)=>{
       body = { success: true, message: 'success', data: response};
   }).catch((error)=>{
       body = { success: false, message: 'error select', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
    })
}


/**
 * @route POST /resepdetailadd
 * @group resep
 * @param {integer} id_resep.query.required - Id Resep
 * @param {integer} id_ingredients.query.required - Id Ingredients
 * @param {integer} total.query.required - Total
 * @param {integer} creator_user.query.required - Creator User
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

exports.resepdetailadd = (req,res)=>{
    var body;
    var dataResepdetail = {
        Id_Resep : req.query.id_resep,
        Id_Ingredients : req.query.id_ingredients,
        Total : req.query.total,
        Creator_user : req.query.creator_user,
        Isdeleted : false
    }
    db('TR_ResepDetail').insert(dataResepdetail)
    .then((response)=>{
       body = { success: true, message: 'success', data: response }
   }).catch((error)=>{
       body = { success: false, message: 'error insert', data: error}
   }).finally(()=>{
       utils.sendStatus(res,200,body);
    })
}

/**
 * @route GET /resepdetaildetails
 * @group resep
 * @param {integer} id_resep.query.required - Id Resep
 * @param {integer} id_ingredients.query.required - Id Ingredients
 * @param {integer} total.query.required - Total
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

exports.resepdetaildetails = (req,res)=>{
    var body;
    var id = req.query.id_resep
    db.from('TR_ResepDetail').select('MS_Resep.Resep_name','MS_Ingredients.Ingredients_name','TR.ResepDetail.Total','MS_Category.Category_name')
    .join('MS_User','MS_Resep.Creator_user','=','MS_User.Id')
    .join('MS_Resep','TR_ResepDetail.Id_Resep','=','MS_Resep.Id')
    .join('MS_Ingredients','TR_ResepDetail.Id_Ingredients','=','MS_Ingredients.Id')
    .join('MS_Category','MS_Resep.Id','=','MS_Category.Id_resep')
    .where('TR_ResepDetail.Id_Resep','=',id)
    .then((response)=>{
       body = { success: true, message: 'success', data: response};
   }).catch((error)=>{
       body = { success: false, message: 'error select', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
    })
}

/**
 * @route PUT /resepdetailupdate
 * @group resep
 * @param {integer} id_resep.query.required - Id Resep
 * @param {integer} id_ingredients.query.required - Id Ingredients
 * @param {integer} total.query.required - Total
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded 
 */

exports.resepdetailupdate = (req,res)=>{
    var body;
    var dtResep = {
     Id_Resep : req.query.id_resep,
     Id_Ingredients : req.query.id_ingredients,
     Total : req.query.total
    } 
    var resep_id = req.query.id_resep;
    var ingredients_id = req.query.id_ingredients;
    db('MS_Resep')
    .where({Id_Resep : resep_id, Id_Ingredients : ingredients_id})
    .update(dtResep)
    .then((response)=>{        
        body = { success: true, message: 'success', data: response};
 
    }).catch((error)=>{
        body = { success: false, message: 'error update', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
    })
    
  }
 
    /**
  * @route DELETE /resepdetaildelete
  * @group resep
  * @param {integer} id_resep.query.required - Id Resep
  * @param {integer} id_ingredients.query.required - Id Ingredients
  * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
  * @produces application/json
  * @consumes application/x-www-form-urlencoded 
  */
 
 exports.resepdetaildelete = (req,res)=>{
    var body;
    var resep_id = req.query.id_resep;
    var ingredients_id = req.query.id_ingredients;
    db('MS_Resep')
    .where({Id_Resep : resep_id, Id_Ingredients : ingredients_id})
    .update(Isdeleted = true)
    .then((response)=>{        
        body = { success: true, message: 'success deleted', data: response};
 
    }).catch((error)=>{
        body = { success: false, message: 'error deleted', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
    })
    
  }