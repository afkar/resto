'use strict';

/**
 * Utils, Library
 */
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var db = require('../../knex/knex');
var utils = require('../utils/utils');
var axios = require('../../tools/axios');
var query = require('querystring');


exports.homelogin = (req,res) => {
    res.render('login/login');
 }

 exports.home = (req,res) => {
    var token = req.cookies.Token;
    var username = req.cookies.Username;
    var userid = req.cookies.Id;
    res.render('layouts/index',{Username : username});
 }

 exports.homeingredients = (req,res) => {
    var token = req.cookies.Token;
    var username = req.cookies.Username;
    var userid = req.cookies.Id;
    var dataarr = [], datax;
    if(token == undefined){
        res.redirect('/index')
    } else{
        axios.get('ingredientsall')
        .then((response)=>{
            dataarr.push({
                dataoutput : response.data.data
            });
            datax = {
                username : username,
                userid : userid,
                ingredients : dataarr
            }
            console.log('data'+datax)
            res.render('ingredients/ingredients',datax);
        })
    }
 }

 exports.homeresep = (req,res) => {
    res.render('resep/resep');
 }

 exports.homecategory = (req,res) => {
    var token = req.cookies.Token;
    var username = req.cookies.Username;
    var userid = req.cookies.Id;
    var dataarr = [], datax;
    if(token == undefined){
        res.redirect('/index')
    } else{
        axios.get('categoryall')
        .then((response)=>{
            dataarr.push({
                dataoutput : response.data.data
            });
            datax = {
                username : username,
                userid : userid,
                category : dataarr
            }
            console.log('data'+datax)
            res.render('category/category',datax);
        })
    }

 }


exports.getResepByCategory = (req, res) =>{
    let category = axios.get('/categoryall')
    var dataarr = [], datax;
    console.log("assas");
    category.then(async (resDataCategory)=> {
                dataarr.push({
                    datacategory :resDataCategory.data.data
                    
                }); 
                datax = {
                    category: dataarr
                }
              
                res.render('/layouts/index', datax);
            })
            
    console.log('date', dataarr.datacategory)
              
}

 exports.registeruser = (req,res) => {     
    var dtInsert = query.stringify({
         username : req.body.username,
         password : req.body.password
     });
     axios.post('/signup?'+dtInsert)
     .then((response)=>{
         if(response.data.success){
             //res.cookies('success',response.data.success);
             //res.render('login/login');
             res.redirect('/index');
         }
     }).catch((error)=>{
         console.log(error)
     })
 }


 exports.signin = (req,res) => {
    var dtInsert = query.stringify({
         username : req.body.usernames,
         password : req.body.passwords
     });
     axios.post('/login?'+dtInsert)
     .then((response)=>{
         if(response.data.success){
             let rsp = response.data.data[0];
             res.cookie('Id',rsp.user.Id);
             res.cookie('Username', rsp.user.Username);
             res.cookie('Token',rsp.token);
             res.redirect('/home');
         }
     }).catch((error)=>{
         console.log(error)
     })
 }


//category
 exports.insertCategory= async  (req, res) =>{

    console.log('dddddddd');
    var dtInsert = query.stringify({
        category_name : req.body.category_name,
        creator_user : req.body.creator_user
    });
    axios.post('/categoryadd?'+dtInsert)
    .then((response)=>{
        if(response.data.success){
            //res.cookies('success',response.data.success);
            //res.render('login/login');
            res.redirect('/homecategory');
        }
    }).catch((error)=>{
        console.log(error)
    })
}


exports.detailCategory = async (req,res) =>{
    var idparam = req.params.id;    
    console.log('idc'+idparam)
    await axios.get('categorydetails?id_category='+idparam)
    .then((response)=>{
        console.log(response.data.data[0]);
        return res.send(response.data.data[0]);
    })
}

exports.updateCategory= async  (req, res) =>{   
    let datas = query.stringify({
        id_category : req.params.id,
        category_name : req.query.category_name
    })
    // console.log(datas);
    
    await axios.put('/categoryupdate?' +datas)
    .then((response) => {
        if(response.data.success == true){
            res.redirect('/homecategory');
        }        
      }).catch((error) => {
        console.log(error.message)
      });
    
}

exports.deleteCategory= async  (req, res) =>{    
    var param = req.params.id;
    console.log("ini param" + param)
    await axios.delete('/categorydelete?id_category='+ param)
    .then((response) => {
        if(response.data.success == true){           
            res.redirect('/homecategory');
        }        
      }).catch((error) => {
        console.log(error.message)
      });
    
}

//ingredients

exports.insertIngredients= async  (req, res) =>{

    var dtInsert = query.stringify({
        ingredients_name : req.body.ingredients_name,
        creator_user : req.body.creator_user
    });
    axios.post('/ingredientsadd?'+dtInsert)
    .then((response)=>{
        if(response.data.success){
            //res.cookies('success',response.data.success);
            //res.render('login/login');
            res.redirect('/homeingredients');
        }
    }).catch((error)=>{
        console.log(error)
    })
}


exports.detailIngredients = async (req,res) =>{
    var idparam = req.params.id;    
    console.log('idc'+idparam)
    await axios.get('Ingredientsdetails?id_ingredients='+idparam)
    .then((response)=>{
        console.log(response.data.data[0]);
        return res.send(response.data.data[0]);
    })
}

exports.updateIngredients= async  (req, res) =>{   
    let datas = query.stringify({
        id_ingredients : req.params.id,
        ingredients_name : req.query.ingredients_name
    })
    // console.log(datas);
    
    await axios.put('/ingredientsupdate?' +datas)
    .then((response) => {
        if(response.data.success == true){
            res.redirect('/homeingredients');
        }        
      }).catch((error) => {
        console.log(error.message)
      });
    
}

exports.deleteIngredients= async  (req, res) =>{    
    var param = req.params.id;
    console.log("ini param" + param)
    await axios.delete('/ingredientsdelete?id_ingredients='+ param)
    .then((response) => {
        if(response.data.success == true){           
            res.redirect('/homeingredients');
        }        
      }).catch((error) => {
        console.log(error.message)
      });
    
}
