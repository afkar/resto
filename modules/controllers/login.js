'use strict';


/**
 * Utils, Library
 */
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var db = require('../../knex/knex');
var utils = require('../utils/utils');
var axios = require('../../tools/axios');
var query = require('querystring');

/**
 * @route POST /login
 * @group Users
 * @param {string} username.query.required - Username
 * @param {string} password.query.required - Password
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

 exports.login = (req,res) => {
     var body;
     db.from('MS_User')
     .where('Username',req.query.username)
     .then((response)=>{
         if(response.length > 0){
          var passwordIsValid = bcrypt.compareSync(req.query.password,response[0].Password);
          if(!passwordIsValid){
              body = { success: false , message: 'Password invalid'}
          } else{
              var token = jwt.sign({
                  id : response[0].Id
              },config.secret,{expiresIn : config.expiresSession});
              var bodyToken= [];
              bodyToken.push({token: token,user: response[0]});

              body = { success:true, message:'success', data:bodyToken };  
          } 
         }else {
            body = { success: false, message:'Username not found!'}
         }

     }).catch((error)=>{
        body = { success: false, message: 'Error select', data: error.message}
    }).finally(()=>{
        utils.sendStatus(res, 200, body);
    })
 }

/**
 * @route POST /signup
 * @group Users
 * @param {string} username.query.required - Username
 * @param {string} password.query.required - Password
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

 exports.signup = (req,res) => {
    console.log("ini " + req.query.password);
    var body;
    var dtinsert = {
        Username : req.query.username,
        Password : bcrypt.hashSync(req.query.password,8),
        IsDeleted : false
    }

    db('MS_User')
    .insert(dtinsert)
    .then((response)=>{
        body = { success: true, message: 'success', data: response};
    }).catch((error)=>{
        body = { success: false, message: 'error select', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
     })
 }

