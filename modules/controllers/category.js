'use strict';


/**
 * Utils, Library
 */
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var db = require('../../knex/knex');
var utils = require('../utils/utils');

/**
 * @route GET /categoryall
 * @group Category
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

 exports.categoryall = (req,res)=>{
     var body;
     db.from('MS_Category').select('MS_Category.Id','MS_Category.Category_name','MS_User.Username')
     .join('MS_User','MS_Category.Creator_user','=','MS_User.Id')
     .where('MS_Category.Isdeleted','=',false)
     .andWhere('MS_User.IsDeleted','=',false)
     .then((response)=>{
        body = { success: true, message: 'success', data: response};
    }).catch((error)=>{
        body = { success: false, message: 'error select', data: error.message};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
     })
 }

 /**
  * @route POST /categoryadd
  * @group Category
  * @param {string} category_name.query.required - Category Name
  * @param {integer} creator_user.query.required - Creator User
  */
 exports.categoryadd = (req,res)=>{
    var body;
    var dataCategory = {
        Category_name : req.query.category_name,
        Creator_user : req.query.creator_user,
        Isdeleted : false
    }
    db('MS_Category').insert(dataCategory)
    .then((response)=>{
       body = { success: true, message: 'success', data: response }
   }).catch((error)=>{
       body = { success: false, message: 'error insert', data: error}
   }).finally(()=>{
       utils.sendStatus(res,200,body);
    })
}

/**
 * @route GET /categorydetails
 * @group Category
 * @param {integer} id_category.query.required - Id Category
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded 
 */

 exports.categorydetails = (req,res)=>{
   var body;
   var id = req.query.id_category;
   db.from('MS_Category').select('MS_Category.Id','MS_Category.Category_name')
   .where('MS_Category.Id','=',id)
   .then((response)=>{        
       body = { success: true, message: 'success', data: response};

   }).catch((error)=>{
       body = { success: false, message: 'error select', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
   })
   
 }

 /**
 * @route PUT /categoryupdate
 * @group Category
 * @param {integer} id_category.query.required - Id Category
 * @param {string} category_name.query.required - Category Name
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded 
 */

exports.categoryupdate = (req,res)=>{
   var body;
   var dtCatgory = {
    Category_name : req.query.category_name
   }; 
   var category_id = req.query.id_category;
   db('MS_Category')
   .where('MS_Category.Id','=',category_id)
   .update(dtCatgory)
   .then((response)=>{        
       body = { success: true, message: 'success', data: response};

   }).catch((error)=>{
       body = { success: false, message: 'error update', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
   })
   
 }

   /**
 * @route Delete /categorydelete
 * @group Category
 * @param {integer} id_category.query.required - Id Category
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded 
 */

exports.categorydelete = (req,res)=>{
   var body;
   var category_id = req.query.id_category;
   db('MS_Category')
   .where('MS_Category.Id','=',category_id)
   .update({Isdeleted : true})
   .then((response)=>{        
       body = { success: true, message: 'success deleted', data: response};

   }).catch((error)=>{
       body = { success: false, message: 'error deleted', data: error};
   }).finally(()=>{
       utils.sendStatus(res,200,body);
   })
   
 }