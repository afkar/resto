'use strict';


/**
 * Utils, Library
 */
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../tools/config');
var db = require('../../knex/knex');
var utils = require('../utils/utils');

/**
 * @route GET /ingredientsall
 * @group Ingredients
 * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
 * @produces application/json
 * @consumes application/x-www-form-urlencoded
 */

 exports.ingredientsall = (req,res)=>{
     var body;
     db.from('MS_Ingredients').select('MS_Ingredients.Id','MS_Ingredients.Ingredients_name','MS_User.Username')
     .join('MS_User','MS_Ingredients.Creator_user','=','MS_User.Id')
     .where('MS_Ingredients.Isdeleted','=',false)
     .andWhere('MS_User.IsDeleted','=',false)
     .then((response)=>{
        body = { success: true, message: 'success', data: response};
    }).catch((error)=>{
        body = { success: false, message: 'error select', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
     })
 }

 /**
  * @route POST /ingredientsadd
  * @group Ingredients
  * @param {string} ingredients_name.query.required - Ingredients Name
  * @param {integer} creator_user.query.required - Creator User
  */
 exports.ingredientsadd = (req,res)=>{
     var body;
     var dataIngredients = {
         Ingredients_name : req.query.ingredients_name,
         Creator_user : req.query.creator_user,
         Isdeleted : false
     }
     db('MS_Ingredients').insert(dataIngredients)
     .then((response)=>{
        body = { success: true, message: 'success', data: response }
    }).catch((error)=>{
        body = { success: false, message: 'error insert', data: error}
    }).finally(()=>{
        utils.sendStatus(res,200,body);
     })
 }

 /**
  * @route GET /ingredientsdetails
  * @group Ingredients
  * @param {integer} id_ingredients.query.required - Id Ingredients
  * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
  * @produces application/json
  * @consumes application/x-www-form-urlencoded 
  */

  exports.ingredientsdetails = (req,res)=>{
    var body;
    db.from('MS_Ingredients').select('MS_Ingredients.Id','MS_Ingredients.Ingredients_name')
    .where('MS_Ingredients.Id','=',req.query.id_ingredients)
    .then((response)=>{        
        body = { success: true, message: 'success', data: response};

    }).catch((error)=>{
        body = { success: false, message: 'error select', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
    })
    
  }

  /**
  * @route PUT /ingredientsupdate
  * @group Ingredients
  * @param {integer} id_ingredients.query.required - Id Ingredients
  * @param {string} ingredients_name.query.required - Ingredients Name
  * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
  * @produces application/json
  * @consumes application/x-www-form-urlencoded 
  */

 exports.ingredientsupdate = (req,res)=>{
    var body;
    var dtIngredients =  {
        Ingredients_name : req.query.ingredients_name
    }
    var ingredients_id = req.query.id_ingredients;
    db('MS_Ingredients')
    .where('MS_Ingredients.Id','=',ingredients_id)
    .update(dtIngredients)
    .then((response)=>{        
        body = { success: true, message: 'success', data: response};

    }).catch((error)=>{
        body = { success: false, message: 'error update', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
    })
    
  }

    /**
  * @route DELETE /ingredientsdelete
  * @group Ingredients
  * @param {integer} id_ingredients.query.required - Id Ingredients
  * @returns {object} 200 - { "success": true, "message": "Message Success, Message Error", "data": "if any, could be object / json" }
  * @produces application/json
  * @consumes application/x-www-form-urlencoded 
  */

 exports.ingredientsdelete = (req,res)=>{
    var body;
    var ingredients_id = req.query.id_ingredients;
    db('MS_Ingredients')
    .where('MS_Ingredients.Id','=',ingredients_id)
    .update({Isdeleted : true})
    .then((response)=>{        
        body = { success: true, message: 'success deleted', data: response};

    }).catch((error)=>{
        body = { success: false, message: 'error deleted', data: error};
    }).finally(()=>{
        utils.sendStatus(res,200,body);
    })
    
  }