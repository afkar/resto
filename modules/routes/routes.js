'use strict';
var config = require('../../tools/config');

module.exports = function(app){
    var login = require('../controllers/login');
    var resep = require('../controllers/resep');
    var ingredients = require('../controllers/ingredients');
    var category = require('../controllers/category');
    var home = require('../controllers/home');

    /* Login */
    app.route('/login').post(login.login);
    app.route('/signup').post(login.signup);

    /* Resep */
    app.route('/resepall').get(resep.resepall);
    app.route('/resepadd').post(resep.resepadd);
    app.route('/resepdetails').get(resep.resepdetails);
    app.route('/resepupdate').put(resep.resepupdate);
    app.route('/resepdelete').delete(resep.resepdelete);
    app.route('/resepbycategory').get(resep.resepbycategory);
    app.route('/resepdetailall').get(resep.resepdetailall);
    app.route('/resepdetailadd').post(resep.resepdetailadd);
    app.route('/resepdetaildetails').get(resep.resepdetaildetails);
    app.route('/resepdetailupdate').put(resep.resepdetailupdate);
    app.route('/resepdetaildelete').delete(resep.resepdetaildelete);

    /* Ingredients */
    app.route('/ingredientsall').get(ingredients.ingredientsall);
    app.route('/ingredientsadd').post(ingredients.ingredientsadd);
    app.route('/ingredientsdetails').get(ingredients.ingredientsdetails);
    app.route('/ingredientsupdate').put(ingredients.ingredientsupdate);
    app.route('/ingredientsdelete').delete(ingredients.ingredientsdelete);
    
    /* Category */
    app.route('/categoryall').get(category.categoryall);
    app.route('/categoryadd').post(category.categoryadd);
    app.route('/categorydetails').get(category.categorydetails);
    app.route('/categoryupdate').put(category.categoryupdate);
    app.route('/categorydelete').delete(category.categorydelete);

    /* Web */
    app.route('/index').get(home.homelogin);

    app.route('/homeingredients').get(home.homeingredients);
    app.route('/homeresep').get(home.homeresep);
    app.route('/homecategory').get(home.homecategory);        
    app.route('/home').get(home.home);

    app.route('/registeruser').post(home.registeruser);
    app.route('/signin').post(home.signin);


    app.route('/insertCategory').post(home.insertCategory);
    app.route('/detailCategory/:id').get(home.detailCategory);
    app.route('/updateCategory/:id').get(home.updateCategory);
    app.route('/deleteCategory/:id').get(home.deleteCategory);


    app.route('/insertIngredients').post(home.insertIngredients);
    app.route('/detailIngredients/:id').get(home.detailIngredients);
    app.route('/updateIngredients/:id').get(home.updateIngredients);
    app.route('/deleteIngredients/:id').get(home.deleteIngredients);


    app.route('/getResepByCategory').get(home.getResepByCategory);
}