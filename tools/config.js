module.exports = {
    'secret': 'resto',
    'versionKey': false,
    'timeoutRedis': 60,
    'expiresSession': 7 * 86400,

    /* server lokal */
    'server' : 'localhost',
    'server_port': 5000,
    'database': 'resto',
    'user': 'workforce_user',
    'password': '12345',
    'host' : '127.0.0.1',
    'port': '5432'
}