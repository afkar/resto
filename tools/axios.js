const axios = require('axios');
const config = require('../tools/config');

const instance = axios.create({
    baseURL : 'http://'+ config.host+':'+ config.server_port + '/',
    proxy : false,
    timeout : 0
});
module.exports = instance