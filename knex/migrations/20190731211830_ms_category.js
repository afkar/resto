
exports.up = function(knex) {
  return knex.schema.createTable('MS_Category',function(table){
      table.increments('Id').unsigned();
      table.string('Category_name');
      table.integer('Id_resep');
      table.foreign('Id_resep').references('MS_Resep.Id');
      table.integer('Creator_user');
      table.foreign('Creator_user').references('MS_User.Id');
      table.boolean('Isdeleted');
  })
};

exports.down = function(knex) {
  let dropDown= 'DROP TABLE MS_Category';
  return knex.raw(dropDown);
};
