
exports.up = function(knex) {
  return knex.schema.createTable('MS_Ingredients',function(table){
    table.increments('Id').unsigned();
    table.string('Ingredients_name');
    table.integer('Creator_user');
    table.foreign('Creator_user').references('MS_User.Id');
    table.boolean('Isdeleted');
  })
};

exports.down = function(knex) {
  let dropDown = 'DROP TABLE MS_Ingredients';
  return knex.raw(dropDown);
};
