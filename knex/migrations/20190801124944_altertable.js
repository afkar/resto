
exports.up = function(knex) {
  return knex.schema.alterTable('MS_Category',function(table){
      table.dropColumn('Id_resep');
  }).then(async()=>{
      await knex.schema.alterTable('MS_Resep',function(table){
          table.integer('Id_category');
          table.foreign('Id_category').references('MS_Category.Id');
      })
  })
};

exports.down = function(knex) {
  
};
