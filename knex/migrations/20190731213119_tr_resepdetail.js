
exports.up = function(knex) {
  return knex.schema.createTable('TR_Resepdetail',function(table){
      table.increments('Id').unsigned();
      table.integer('Id_Resep');
      table.foreign('Id_Resep').references('MS_Resep.Id');
      table.integer('Id_Ingredients');
      table.foreign('Id_Ingredients').references('MS_Ingredients.Id');
      table.integer('Total');
      table.integer('Creator_user');
      table.foreign('Creator_user').references('MS_User.Id');
      table.boolean('Isdeleted');
  })
};

exports.down = function(knex) {
  let dropDown = 'DROP TABLE TR_Resepdetail';
  return knex.raw(dropDown);
};
