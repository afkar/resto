
exports.up = function(knex) {
  return knex.schema.createTable('MS_Resep',function(table){
        table.increments('Id').unsigned();
        table.string('Resep_name');
        table.integer('Creator_user');
        table.foreign('Creator_user').references('MS_User.Id');
        table.boolean('Isdeleted');
  })
};

exports.down = function(knex) {
  let dropDown = 'DROP TABLE MS_Resep';
  return knex.raw(dropDown);
};
