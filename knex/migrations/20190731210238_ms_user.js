
exports.up = function(knex) {
  return knex.schema.createTable('MS_User',function(table){
    table.increments('Id').unsigned();
    table.string('Username');
    table.string('Password');
    table.boolean('IsDeleted');
  })
};

exports.down = function(knex) {
  let dropQuery = 'DROP TABLE MS_User';
  return knex.raw(dropQuery);
};
