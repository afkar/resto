
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('MS_User').del()
    .then(function () {
      // Inserts seed entries
      return knex('MS_User').insert([
        {Id: 1, Username:'staff',Password:'$2y$08$/veIiq6kcazGGY2rmUtt9uBu8D9qJWQsudi356CFRyYJz/2nU4TGu',IsDeleted:false},
        {Id: 2, Username: 'admin',Password:'$2y$08$l7VqedBc9eHAHxEBGaZ2WuxOD8.PREnlgfpG84nhI7OOiJ3/jMa26',IsDeleted:false},
      ]);
    });
};
